Plug 'junegunn/vim-plug'
"" Vim Plug
"" General
Plug 'pbrisbin/vim-mkdir'
Plug 'tpope/vim-eunuch'
"" Language support
""" General
Plug 'sheerun/vim-polyglot'
""" Markdown syntaxis
Plug 'tpope/vim-markdown'
Plug 'plasticboy/vim-markdown'
""" HTML and CSS editing
Plug 'othree/html5.vim'
Plug 'hail2u/vim-css3-syntax'
Plug 'alvan/vim-closetag'
Plug 'vim-scripts/closetag.vim'
Plug 'mattn/emmet-vim'
Plug 'slim-template/vim-slim'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'cakebaker/scss-syntax.vim'
Plug 'gko/vim-coloresque'
""" Javascript
Plug 'elzr/vim-json'
Plug 'leafgarland/typescript-vim'
Plug 'Quramy/tsuquyomi'
""" Ruby
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rake'
Plug 'tpope/vim-rails'
"" IDE-like plugins
Plug 'editorconfig/editorconfig-vim'
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'justinmk/vim-sneak'
Plug 'haya14busa/incsearch.vim'
Plug 'brooth/far.vim'
Plug 'tpope/vim-surround'
Plug 'matze/vim-move'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
""" Completion system
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins'  }
Plug 'zchee/deoplete-jedi'
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'
Plug 'honza/vim-snippets'
Plug 'raimondi/delimitmate'
""" CtrlP
Plug 'ctrlpvim/ctrlp.vim'
Plug 'FelikZ/ctrlp-py-matcher'
""" Graphic interface
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'reedes/vim-pencil'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'terryma/vim-smooth-scroll'
Plug 'bling/vim-bufferline'
Plug 'majutsushi/tagbar'
Plug 'Yggdroot/indentLine'
"" Git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'Xuyuanp/nerdtree-git-plugin'
"" Python
Plug 'jmcantrell/vim-virtualenv'
Plug 'ehamberg/vim-cute-python'

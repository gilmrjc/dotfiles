#!/bin/sh

create_symlink() {

    # Check if the symlink is set
    if [ "$(readlink "$target_file")" = "$source_file" ]; then
        print_success "$symlink_message"
        # If there is no file, create the symlink
    elif [ ! -e "$target_file" -a ! -L "$target_file" ]; then
        execute "ln -s \"$source_file\" \"$target_file\"" "$symlink_message"
        # Suppose the file exists
    else
        # Check if it's needed to ask before deleting the file
        if [ $skipQuestions = false ]; then
            ask_for_confirmation \
                "'$target_file' already exists, de you want to overwrite it?"
        fi
        # Check confirmation to delete the file and symlink it
        if [ $skipQuestions = true ] || answer_is_yes; then
            rm -rf "$target_file"
            execute "ln -s \"$source_file\" \"$target_file\"" "$symlink_message"
            # The file couldn't be overwritten
        else
            print_error "$target_file already exists"
        fi
    fi
}

create_symlinks() {

    # Define the array of symlinks
    set -- "neovim/nvimrc" ".config/nvim/init.vim" \
            "neovim/plugins.vim" ".config/nvim/plugins.vim" \
            "zsh/zshrc" ".zshrc" \
            "gemrc" ".gemrc" \
            "zsh/functions" ".marsbot/functions" \
            "zsh/completions" ".marsbot/completions" \
            "zsh/aliases.zsh" ".marsbot/aliases.zsh" \
            "git/gitmessage" ".gitmessage" \
            "git/gitconfig" ".gitconfig" \
            "git/gitignore" ".gitignore"

    # Transverse the array with a pair of file names per loop
    while [ $# -ge 2 ]; do
        source_file_name="$1"
        target_file_name="$2"
        source_file="$SCRIPT_PATH/$1"
        target_file="$HOME/$2"
        symlink_message="~/$target_file_name → $source_file_name"

        create_symlink

        shift 2
    done
}

main() {

    print_title "Create symbolic links"
    create_symlinks
}

main

#!/bin/sh

main() {

    # Name used to call this script
    caller=$0
    # Variables used to take decisions
    skipInstall=false
    skipQuestions=false
    skipSymlinks=false

    # If this script is called from the command line assume that we are
    # updating the system, otherwise assume it's called directly from the
    # internet to bootstrap the system.
    echo "$0"
    if echo "$0" | grep -q "bootstrap.sh"; then
        print_in_red "\nUpdating the system\n"
    else
        print_in_red "\nBootstraping the system\n"
    fi

    # Parse the command line options
    while :; do
        case $1 in
            --skip-install) skipInstall=true;;
            --skip-symlinks) skipSymlinks=true;;
            -y|--yes) skipQuestions=true;;
            *) break;;
        esac
        shift 1
    done

    # Load the utils or abort
    if [ -f "utils.sh" ]; then
        . "utils.sh" || exit 1
    else
        echo "Error, please download all files" &&  exit 1
    fi

    # Look if the current os is supported
    verify_os || exit 1

    # Get the absolute path to the current directory
    # The path is stored in $SCRIPT_PATH
    get_current_directory "$caller"

    # Ask for sudo once, and use it for all the script
    ask_sudo

    # Create required directories
    . "create_directories.sh"

    # Install software; skip with --skip-install option
    if [ $skipInstall = false ]; then
        echo "install"
        # . "installs.sh" "$os_name"
    fi

    # Create symlinks; skip with --skip-symlinks
    if [ $skipSymlinks = false ]; then
        echo "symlink"
        # . "create_symlinks.sh"
    fi

    # . "shell.sh"
    # zsh "setup.zsh"
}

main "$@"

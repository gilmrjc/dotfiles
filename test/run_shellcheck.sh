#!/bin/sh

main() {
    shellcheck -x "main.sh"
    print_result $? "Run tests through ShellCheck"
    cd ".."
    shellcheck -x "bootstrap.sh"
    print_result $? "Run bootstrap through ShellCheck"
}

main

#!/bin/sh

print_title "Install software"

# Install macOS software
if [ "$OS_NAME" = "macOS" ]; then
    . "./macOS/main.sh"
fi

. "./fonts.sh"

# Install nodeJS using nvm
. "./nvm.sh"

# Install Python using pyenv
. "./pyenv.sh" "macOS"

# Install Ruby using rbenv
. "./rbenv.sh"

#!/bin/sh

create_directories() {

    # Define the array of directories
    set -- "$HOME/projects" \
            "$HOME/projects/marsbot" \
            "$HOME/projects/marsbot-setup/master" \
            "$HOME/.config/nvim" \
            "$HOME/.config/git" \
            "$HOME/.marsbot"

    # Create all the directories
    for i in "$@"; do
        mkd "$i"
    done
}

main() {

    print_title "Create directories"
    create_directories
}

main

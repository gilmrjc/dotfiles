#!/bin/sh

change_shell() {

    SHELL_PATH="$(which zsh)"
    if [ $SHELL = $SHELL_PATH ]; then
        print_success "Setting zsh as shell"
    elif $(grep -q "$SHELL_PATH" /etc/shells); then
        execute "$(chsh -s "$SHELL_PATH")" "Setting zsh as shell"
    else
        echo "$SHELL_PATH" | sudo tee -a /etc/shells
        execute "$(chsh -s "$SHELL_PATH")" "Setting up zsh as shell"
    fi
}

source_zshrc() {

    execute "zsh $HOME/.zshrc" "Sourcing zshrc"
}

main() {

    print_title "Configure shell"

    change_shell
    source_zshrc
}

main

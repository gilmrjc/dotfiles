#!/bin/sh

activate_nvm() {

    NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
}

install_latest_stable_lts_node() {

    execute "nvm install --lts" "Installing latest stable lts node"
}

install_latest_stable_node() {

    execute "nvm install node" "Installing latest stable node"
}

set_latest_stable_lts_node() {

    execute "nvm use --lts" "Set last stable lts node as default"
}

main() {

    print_subtitle "NVM"

    activate_nvm
    install_latest_stable_lts_node
    install_latest_stable_node
    set_latest_stable_lts_node
}

main

#!/bin/sh

install_python() {

    # Add pyenv to the path
    if [ -d "$HOME/.pyenv/bin" ]; then
        export PATH="$HOME/.pyenv/bin:$PATH"
    fi

    # Activate it
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"

    # Add compilation flags
    if [ "$1" = "macOS" ]; then
        export CFLAGS="-I$(brew --prefix readline)/include -I$(brew --prefix openssl)/include -I$(xcrun --show-sdk-path)/usr/include"
        export LDFLAGS="-L$(brew --prefix readline)/lib -L$(brew --prefix openssl)/lib"
        export PYTHON_CONFIGURE_OPTS="--enable-shared --enable-unicode=ucs2"
    else
        export PYTHON_CONFIGURE_OPTS="--enable-shared"
    fi

    # Install all supported Python versions
    execute "pyenv install -s 2.7.13" "Installing Python 2.7"
    execute "pyenv install -s 3.3.6" "Installing Python 3.3"
    execute "pyenv install -s 3.4.6" "Installing Python 3.4"
    execute "pyenv install -s 3.5.3" "Installing Python 3.5"
    execute "pyenv install -s 3.6.0" "Installing Python 3.6"
    execute "pyenv install -s pypy-5.6.0" "Installing PyPy 5.6"
    pyenv rehash
}

set_default_python() {

    execute "pyenv global 3.6.0" "Set Python 3.6 as default"
}

main() {

    print_subtitle "Pyenv"

    install_python
    set_default_python
}

main

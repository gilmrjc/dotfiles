#!/bin/sh

install_ruby() {

    # Add rbenv to the path
    if [ -d "$HOME/.rbenv/bin" ]; then
        export PATH="$HOME/.rbenv/bin:$PATH"
    fi

    # Activate it
    eval "$(rbenv init -)"

    # Install supported Ruby versions
    execute "rbenv install -s 2.1.10" "Installing Ruby 2.1"
    execute "rbenv install -s 2.2.6" "Installing Ruby 2.2"
    execute "rbenv install -s 2.3.3" "Installing Ruby 2.3"
    execute "rbenv install -s 2.4.0" "Installing Ruby 2.4"
    rbenv rehash
}

set_default_ruby() {

    execute "rbenv global 2.4.0" "Set Ruby 2.4 as default"
}

main() {

    print_subtitle "Rbenv"

    install_ruby
    set_default_ruby
}

main
